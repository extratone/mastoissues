# [\#22144 Issue](https://github.com/mastodon/mastodon/issues/22144) `open`: tootctl feeds build: list feeds are not populated as documented
**Labels**: `bug`


#### <img src="https://avatars.githubusercontent.com/u/34654503?u=ac56a3e9a4878c683dea6a70b66e42b6ff4307e9&v=4" width="50">[EqualMa](https://github.com/EqualMa) opened issue at [2022-12-08 03:21](https://github.com/mastodon/mastodon/issues/22144):

### Steps to reproduce the problem

1. `tootctl feeds clear`
2. `tootctl feeds build --all`


### Expected behaviour

 Home and list feeds are both built.

### Actual behaviour

Only home feeds are built. List feeds are now empty.

### Detailed description

I upgraded Mastodon to 4.0, which also upgrades Redis 6 to 7.
Then I run `tootctl feeds clear` and `tootctl feeds build --all` as suggested by #19063.
The home feeds are re-created correctly, but the list feeds are cleared and not re-created.

### Specifications

Mastodon 4.0.2
Ruby 3.0.4p208
Node.js v16.17.1

#### <img src="https://avatars.githubusercontent.com/u/384364?u=4fc3824b37944fc752160718eee736cecde57a67&v=4" width="50">[ClearlyClaire](https://github.com/ClearlyClaire) commented at [2022-12-08 11:21](https://github.com/mastodon/mastodon/issues/22144#issuecomment-1342564211):

It seems like the command's documentation started mentioning lists in #8675 but that was never implemented.


-------------------------------------------------------------------------------



[Export of Github issue for [mastodon/mastodon](https://github.com/mastodon/mastodon). Generated on 2023.10.20 at 09:17:47.]
