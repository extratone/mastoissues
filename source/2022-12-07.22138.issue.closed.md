# [\#22138 Issue](https://github.com/mastodon/mastodon/issues/22138) `closed`: SSO Integration and Authorization of third-party apps is blocked by the new form-action CSP policy
**Labels**: `bug`


#### <img src="https://avatars.githubusercontent.com/u/31551856?u=8722da1d7a44adfed768a6e302e2b113e2b6c339&v=4" width="50">[CSDUMMI](https://github.com/CSDUMMI) opened issue at [2022-12-07 21:32](https://github.com/mastodon/mastodon/issues/22138):

### Steps to reproduce the problem

1.  Setup a mastodon instance running the source code currently in `main`
2.  Go to [pinafore.social](https://pinafore.social) and login via your instance in Chrome or a Chromium-based browser.
3. OR login via SSO in a Chrome or a Chromium-based browser

### Expected behaviour

Authorizing third-party apps and using SSO logins should work.

### Actual behaviour

Requests redirecting to the third-party app (Authorizations) or to the SSO provider are blocked

### Detailed description

Chrome requires redirects after a form action to comply with the form-action CSP policy, despite the W3C not defining a standardized behavior for these.[^1]

This bug is caused by the PR [#20781](https://github.com/mastodon/mastodon/pull/20781) adding a `form-action: 'self'` policy without discussing the potential side-effects of this policy change on features such as third-party apps or SSO. 

[^1]: [ CSP:form-action, MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/form-action)



### Specifications

Mastodon's main branch build from source
Chrome browser and other Chromium-based browsers (e.g. Brave)

#### <img src="https://avatars.githubusercontent.com/u/384364?u=4fc3824b37944fc752160718eee736cecde57a67&v=4" width="50">[ClearlyClaire](https://github.com/ClearlyClaire) commented at [2022-12-08 10:33](https://github.com/mastodon/mastodon/issues/22138#issuecomment-1342494358):

Does your instance includes commits 4ae97a2e4c4bea850c95a523e84e0424e7c18ffd (PR #20958) and 48e136605a30fa7ee71a656b599d91adf47b17fc (PR #20962)?

I don't have SSO enabled, so it is a pain to try, but I'm not able to reproduce the issue with pinafore and SSO disabled.

#### <img src="https://avatars.githubusercontent.com/u/31551856?u=8722da1d7a44adfed768a6e302e2b113e2b6c339&v=4" width="50">[CSDUMMI](https://github.com/CSDUMMI) commented at [2022-12-08 12:12](https://github.com/mastodon/mastodon/issues/22138#issuecomment-1342636105):

I have not. It appears to fix the issue with third-party apps, but does not fix the SSO Login issue.

My work-around for that later issue was introducing a novel environment variable for the domain of the SSO provider to append to the form-action csp. 

But this solution is ugly, because it requires further configuration. And although I'm not a very proficient reader and writer of Ruby on Rails code, I'd think that applying the same patch to SSO that has been applied on the oauth authorization would be more difficult, because the sign in and sign up controllers are handled by devise and not mastodon directly.

#### <img src="https://avatars.githubusercontent.com/u/31551856?u=8722da1d7a44adfed768a6e302e2b113e2b6c339&v=4" width="50">[CSDUMMI](https://github.com/CSDUMMI) commented at [2022-12-08 12:13](https://github.com/mastodon/mastodon/issues/22138#issuecomment-1342636744):

Was the form action CSP policy introduced in response to any actual vulnerability in Mastodon?

#### <img src="https://avatars.githubusercontent.com/u/384364?u=4fc3824b37944fc752160718eee736cecde57a67&v=4" width="50">[ClearlyClaire](https://github.com/ClearlyClaire) commented at [2022-12-08 12:55](https://github.com/mastodon/mastodon/issues/22138#issuecomment-1342691137):

Thanks for the input. I need to set up a SSO environment to test that and write a fix. Do you have a recommendation for something very simple to set up?

> Was the form action CSP policy introduced in response to any actual vulnerability in Mastodon?

Kind of. A vulnerability was found in Mastodon and forks that could be exploited to insert HTML in posts: https://portswigger.net/research/stealing-passwords-from-infosec-mastodon-without-bypassing-csp

While I have seen no evidence that it could be exploited for anything truly malicious in Mastodon itself, it was possible to get some forks to render arbitrary forms, and thus steal passwords in some circumstances with minimal user interaction.

That vulnerability has been fixed, but the exploit could have been prevented by using an adequate `form-action` policy.

#### <img src="https://avatars.githubusercontent.com/u/31551856?u=8722da1d7a44adfed768a6e302e2b113e2b6c339&v=4" width="50">[CSDUMMI](https://github.com/CSDUMMI) commented at [2022-12-08 14:12](https://github.com/mastodon/mastodon/issues/22138#issuecomment-1342798777):

I'm running a test setup on a fork of glitch-soc that I maintain [here](https://gitlab.com/babka_net/mastodon-babka).

As an IdP I use [keycloak](https://keycloak.org).

If you want, I can share the minimal docker-compose file I use to set that up & the .env.production file I use (with the sensitive info removed) to connect with keycloak using OIDC.

#### <img src="https://avatars.githubusercontent.com/u/384364?u=4fc3824b37944fc752160718eee736cecde57a67&v=4" width="50">[ClearlyClaire](https://github.com/ClearlyClaire) commented at [2022-12-08 14:14](https://github.com/mastodon/mastodon/issues/22138#issuecomment-1342801707):

I just set up a dev keycloak server as SAML identity provider and could log in without issue using Chromium. Can you describe exactly which step fails?

#### <img src="https://avatars.githubusercontent.com/u/31551856?u=8722da1d7a44adfed768a6e302e2b113e2b6c339&v=4" width="50">[CSDUMMI](https://github.com/CSDUMMI) commented at [2022-12-08 14:24](https://github.com/mastodon/mastodon/issues/22138#issuecomment-1342816727):

When I clicked on the login button, the button caused a CSP violation due to the SSO provider not being included under the `form-action: 'self';` policy.

I fixed this by having every CSP header sent by mastodon globally include the `form-action: 'self' <my SSO provider's domain>`.

#### <img src="https://avatars.githubusercontent.com/u/384364?u=4fc3824b37944fc752160718eee736cecde57a67&v=4" width="50">[ClearlyClaire](https://github.com/ClearlyClaire) commented at [2022-12-08 14:40](https://github.com/mastodon/mastodon/issues/22138#issuecomment-1342837416):

> When I clicked on the login button, the button caused a CSP violation due to the SSO provider not being included under the `form-action: 'self';` policy.
> 
> I fixed this by having every CSP header sent by mastodon globally include the `form-action: 'self' <my SSO provider's domain>`.

But with 48e136605a30fa7ee71a656b599d91adf47b17fc, `/auth/sign_in` does not set the `form-action` directive. Can you revert your change and double-check that the CSP does not include a `form-action` directive on that page?

#### <img src="https://avatars.githubusercontent.com/u/31551856?u=8722da1d7a44adfed768a6e302e2b113e2b6c339&v=4" width="50">[CSDUMMI](https://github.com/CSDUMMI) commented at [2022-12-08 15:14](https://github.com/mastodon/mastodon/issues/22138#issuecomment-1342885562):

I will do that.

#### <img src="https://avatars.githubusercontent.com/u/31551856?u=8722da1d7a44adfed768a6e302e2b113e2b6c339&v=4" width="50">[CSDUMMI](https://github.com/CSDUMMI) commented at [2022-12-08 18:55](https://github.com/mastodon/mastodon/issues/22138#issuecomment-1343187961):

Thank you. 

I rebased on glitch's current main and now it's working. 

I'll close this issue.


-------------------------------------------------------------------------------



[Export of Github issue for [mastodon/mastodon](https://github.com/mastodon/mastodon). Generated on 2023.10.20 at 09:17:47.]
